﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProiectPIU
{
    public partial class Form1 : Form
    {
        private const int NUME = 0;
        private const int CLASIFICARE = 1;
        private const int PRET = 2;
        private const int CANT = 3;
        private const int RETETA = 4;
        private const int FABRIC = 5;
        private const int EXP = 6;
        List<Medicament> ListaStocare = new List<Medicament>();
        string NumeFisier { get; set; }
        public Form1()
        {
            InitializeComponent();
            //ADAUGARE DATE DIN FISIER
            ListaStocare = ListaStocareFisier();
        }
        //CITIRE ELEMENTE DIN FISIER
        private List<Medicament> ListaStocareFisier()
        {
            //NumeFisier = "c:\\users\\enach\\onedrive\\desktop\\proiectpiu\\StocareFisier.txt"; //Fisier hard codat!!!
            //FISIER CALE RELATIVA!!!
            NumeFisier = "StocareTextPath.txt"; //L am facut in bin -> debug -> apelez doar cu numele!!!
            List<Medicament> StocareFisier = new List<Medicament>();
            string line = string.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(NumeFisier))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] ElementeMed = line.Split(';');
                        StocareFisier.Add(new Medicament(ElementeMed[NUME], ElementeMed[CLASIFICARE], Int32.Parse(ElementeMed[PRET]), Int32.Parse(ElementeMed[CANT]), ElementeMed[RETETA], DateTime.Parse(ElementeMed[FABRIC]), DateTime.Parse(ElementeMed[EXP])));
                    }
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Eroare fisier!");
            }
            return StocareFisier;
        }
        //SCRIERE ELEMENTE IN FISIER
        private void ActualizareFisier()
        {
            try
            {
                using (StreamWriter swFisierText = new StreamWriter(NumeFisier,false))
                {
                    foreach (Medicament element in ListaStocare)
                        swFisierText.WriteLine(element.SirPentruFisier());
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Eroare actualizare fisier!!!");
            }
        }
        //BUTON ADAUGA
        private void button1_Click(object sender, EventArgs e)
        {
            Medicament medicament = new Medicament();
            //NUME
            if(textBox1.Text == string.Empty)
            {
                MessageBox.Show("Adaugati numele medicamentului!");
                return;
            }
            else
            {
                medicament.NumeMedicament = textBox1.Text;
            }
            //CANTITATE
            if(textBox2.Text == string.Empty || !(Int32.TryParse(textBox2.Text, out int x)))
            {
                MessageBox.Show("Adaugati o cantitate valida!");
                return;
            }
            else
            {
                medicament.CantitateMedicament = x;
            }
            //PRET
            if (textBox4.Text == string.Empty || !(Int32.TryParse(textBox4.Text, out x)))
            {
                MessageBox.Show("Adaugati un pret valid!");
                return;
            }
            else
            {
                medicament.PretMedicament = x;
            }
            //CLASIFICARE
            if (radioButton1.Checked)
                medicament.ClasificareMedicament = radioButton1.Text;
            else if (radioButton2.Checked)
                medicament.ClasificareMedicament = radioButton2.Text;
            else if (radioButton3.Checked)
                medicament.ClasificareMedicament = radioButton3.Text;
            else if (radioButton4.Checked)
                medicament.ClasificareMedicament = radioButton4.Text;
            else if (radioButton5.Checked)
                medicament.ClasificareMedicament = radioButton5.Text;
            else if (radioButton6.Checked)
                medicament.ClasificareMedicament = radioButton6.Text;
            else
            {
                label4.ForeColor = Color.Red;
                MessageBox.Show("Selectati o categorie!!!");
                return;
            }
            label4.ForeColor = Color.Black;
            //RETETA
            if (checkBox1.Checked)
                medicament.Reteta = checkBox1.Text;
            else if (checkBox2.Checked)
                medicament.Reteta = checkBox2.Text;
            else
            {
                label8.ForeColor = Color.Red;
                MessageBox.Show("Precizati daca este sau nu necesara reteta!");
                return;
            }
            label8.ForeColor = Color.Black;
            //DATA
            medicament.DataFabricare = dateTimePicker1.Value;
            medicament.DataExpirare = dateTimePicker2.Value;
            //ADAUGARE IN LISTA
            ListaStocare.Add(medicament);
            ActualizareFisier();
            //CURATARE
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox4.Text = string.Empty;
            radioButton1.Checked = false; radioButton2.Checked = false; radioButton3.Checked = false; radioButton4.Checked = false; radioButton5.Checked = false; radioButton6.Checked = false;
            checkBox1.Checked = false; checkBox2.Checked = false;
        }
        //AFISARE
        private void button2_Click(object sender, EventArgs e)
        {
            if (ListaStocare.Count == null)
            {
                MessageBox.Show("Nu exista elemente de afisat!");
            }
            else
            {
                listBox1.Items.Clear();
                foreach (Medicament medicament in ListaStocare)
                {
                    listBox1.Items.Add(medicament.MedicamentSir());
                }
            }
        }
        //STERGE
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ListaStocare.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.Clear();
                foreach (Medicament medicament in ListaStocare)
                {
                    listBox1.Items.Add(medicament.MedicamentSir());
                }
                ActualizareFisier();
            }
            catch(Exception)
            {
                MessageBox.Show("Selectati un element!");
                return;
            }
        }
        //MODIFICA
        private void button4_Click(object sender, EventArgs e)
        {
            Medicament medModif = new Medicament();
            try
            {
                for (int i = 0; i <= ListaStocare.Count; i++)
                {
                    if (listBox1.SelectedIndex == i)
                    {
                        medModif = ListaStocare.ElementAt(i);//selectare medicament
                        ListaStocare.RemoveAt(i);//stergere versiune veche din lista
                    }
                }
                //AUTOFILL CAMPURI
                textBox1.Text = medModif.NumeMedicament;
                textBox2.Text = medModif.CantitateMedicament.ToString();
                textBox4.Text = medModif.PretMedicament.ToString();
                MessageBox.Show("Realizati modificarile dorite si apasati Adauga!");
                ActualizareFisier();
            }
            catch (Exception)
            {
                MessageBox.Show("Selectati un element!");
                return;
            }
        }
        //DETALII
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.Text = string.Empty;
                for (int i = 0; i <= ListaStocare.Count; i++)
                {
                    if (listBox1.SelectedIndex == i)
                    {
                        richTextBox1.Text = ListaStocare.ElementAt(i).DetaliiMedicamentSir();
                    }
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Selectati un element!");
                return;
            }
        }
        //CAUTA
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                bool gasit = false;
                foreach (Medicament medicament in ListaStocare)
                {
                    if (medicament.NumeMedicament == textBox3.Text)
                    {
                        listBox1.SelectedIndex = ListaStocare.IndexOf(medicament);
                        richTextBox1.Text = string.Empty;
                        richTextBox1.Text = medicament.DetaliiMedicamentSir();
                        gasit = true;
                        return;
                    }
                }
                if (!gasit)
                {
                    richTextBox1.Text = string.Empty;
                    richTextBox1.Text = "Medicamentul nu a fost gasit!";
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Selectati un element!");
                return;
            }
        }
    }
}
