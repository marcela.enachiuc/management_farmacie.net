﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectPIU
{
    class Medicament
    {
        public string NumeMedicament;
        public string ClasificareMedicament;
        public string Reteta;
        public int CantitateMedicament;
        public int PretMedicament;
        public DateTime DataFabricare;
        public DateTime DataExpirare;
        //CONSTRUCTOR FARA PARAMETRI
        public Medicament() { }
        //CONSTRUCTOR CU PARAMETRI
        public Medicament(string nume, string clasificare,int pret, int cantitate, string reteta, DateTime fabricare, DateTime expirare)
        {
            NumeMedicament = nume;
            ClasificareMedicament = clasificare;
            Reteta = reteta;
            CantitateMedicament = cantitate;
            PretMedicament = pret;
            DataFabricare = fabricare;
            DataExpirare = expirare;
        }
        //CONSTRUCTOR CU PARAMETRI
        public Medicament(string nume, string clasificare, int pret, int cantitate, string reteta)
        {
            NumeMedicament = nume;
            ClasificareMedicament = clasificare;
            Reteta = reteta;
            CantitateMedicament = cantitate;
            PretMedicament = pret;
            DataFabricare = DateTime.Now;
            DataExpirare = DateTime.Now;
        }
        public string MedicamentSir()
        {
            string s = string.Empty;
            s = $"{NumeMedicament}";
            return s;
        }
        public string DetaliiMedicamentSir()
        {
            string s = string.Empty;
            s = $"NUME: {NumeMedicament}\nPRET: {PretMedicament}\nCLASIFICARE: {ClasificareMedicament}\nCANTITATE: {CantitateMedicament}\nRETETA: {Reteta}\nDATA FABRICARII: {DataFabricare}\nVALABIL PANA LA: {DataExpirare}";
            return s;
        }
        public string SirPentruFisier()
        {
            string s = string.Empty;
            s = $"{NumeMedicament};{ClasificareMedicament};{PretMedicament};{CantitateMedicament};{Reteta};{DataFabricare};{DataExpirare};";
            return s;
        }
    }
}
